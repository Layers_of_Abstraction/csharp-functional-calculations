﻿using System;

namespace ForwardFunctionalComposition
{    
    public static class FuncExtensions
    {        
        //A multi-parameter add function broken down into one that expects only one paramater at a time. 
        public static Func<TArg1, Func<TArg2, Func<TArg3, TResult>>> Curry<TArg1, TArg2, TArg3,  TResult>(
            this Func<TArg1, TArg2, TArg3, TResult> func)
        {
            return arg1 => arg2 => arg3 => func(arg1, arg2, arg3);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Currying 3 paramater function
            Func<int, int, int, int> add = (x, y, z) => x + y + z;
            var curredAdd = add.Curry();
            // Apply one paramater at a time
            var partialResult = curredAdd(3)(4)(5);
            
            Console.WriteLine(partialResult);
        }
    }
}
